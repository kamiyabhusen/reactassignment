import React, { useState } from 'react'
import { Redirect } from 'react-router-dom';
import usersJSON from '../../data/users.json';
import hashPassword from '../../utils/hashPassword';

export default function Login() {

    const [email,setEmail] = useState("");
    const [password,setPassword] = useState("");
    const [error, setError] = useState("")
    const [redirect, setRedirect] = useState(false)


    const handleSubmit = async  (e) => {
        setError("");
        e.preventDefault();

        let { users } = usersJSON;
        let loggedInUser = null;
        
        //check user array for right email and password
        const isLoggedIn = users.some((user) => {
            if((user.email === email) && (user.password === password)){
                loggedInUser = user;
                return true
            }else{
                return false
            }
        });

        //if return false it means email and password wrong
        if(!isLoggedIn){
           return setError("Email or Password Invalid");
        }

        //hashing the password
        let hashedPassword = await hashPassword(loggedInUser.password);

        //store in localstorage in json formate
        localStorage.setItem("user",JSON.stringify({
            ...loggedInUser,
            password:hashedPassword
        }));

        //set redirect true for redirect to home page
        setRedirect(true);

    }


    //redirect when user is in localstorage or redirect is true
    if(localStorage.user || redirect){
        return <Redirect to="/" />
    }

    return (
        <div className="container">
            <h1>Login</h1>
            <form onSubmit={handleSubmit}>
                {error && <p style={{color:"red"}}>{error}</p>}
                <div>
                    <label htmlFor="username">Email: </label><br/>
                    <input 
                        type="email"
                        onChange={(e) => setEmail(e.target.value)} 
                        required
                    />
                </div>
                <div>
                    <label htmlFor="password">Password: </label><br/>
                    <input 
                        type="password" 
                        required
                        onChange={(e) => setPassword(e.target.value)}
                    />
                </div>
                <div style={{textAlign:'center',marginTop:"1rem"}}>
                    <input type="submit" value="Login"/>
                </div>
            </form>
        </div>
    )
}
