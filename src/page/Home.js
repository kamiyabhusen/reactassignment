import React from 'react'
import { Redirect } from 'react-router-dom'
import List from '../components/List';
import invitationsJSON from '../data/invitations.json';
import invitationsUpdateJSON from '../data/invitations_update.json';


class Home extends React.Component {
    constructor(){
        super();
        this.state = {
            invites:[],
            updateInvites:[],
            user:{},
            intervalid:'',
            redirect:false,
        }
        this.updateByInterval = this.updateByInterval.bind(this);
    }

    componentDidMount(){

        //checking if user is in localstorage or not
        if(!localStorage.user){
            this.setState({redirect:true});
        }else{

            //load user from localstorage and store in state
            let currentUser = JSON.parse(localStorage.getItem("user"))
            this.setState({user:currentUser});

            //filter invites according to user id
            let { invites:invitations } = invitationsJSON
            let invitesById = invitations.filter((invitation) => invitation.user_id == currentUser.user_id);
            
            this.setState({invites:invitesById});

            //fitering update invite from invitationupdateJSON
            let { invites:invitationsUpdates } = invitationsUpdateJSON;
            let updateInvitesById = invitationsUpdates.filter((invitation) => invitation.user_id == currentUser.user_id );

            this.setState({updateInvites:updateInvitesById});

            const interval = setInterval(this.updateByInterval,5000);
            this.setState({intervalid: interval});
        }
    }

    componentWillUnmount(){
        clearInterval(this.state.intervalid);
    }

    updateByInterval(){
        if(this.state.updateInvites.length == 1){
            clearInterval(this.state.intervalid);
        }

        //making copy of state
        let updateInvitesArr = [...this.state.updateInvites];
        let update = updateInvitesArr.shift();
        this.setState({updateInvites:updateInvitesArr});
        this.setState({invites:[...this.state.invites,update]});
    }

    handleLogout = () => {
       localStorage.removeItem("user");
       this.setState({redirect:true});
    }

    render(){

        if(this.state.redirect){
            return <Redirect to="/login" />
        }

        return (
            <div>
                <h1>hello {this.state.user.first_name}</h1>
                <p>Here it is your notifications</p>
                <button onClick={this.handleLogout}>Logout</button>
                <List invites={this.state.invites} />
            </div>
        )
    }
}

export default Home;