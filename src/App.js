import { BrowserRouter as Router,Switch,Route } from 'react-router-dom'
import './App.css';

//pages
import LoginPage from './page/auth/Login';
import HomePage from './page/Home';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/login" exact component={LoginPage} />
        <Route path="/" exact component={HomePage} />
      </Switch>
    </Router>
  );
}

export default App;
