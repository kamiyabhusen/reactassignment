import React from 'react'

export default function List(props) {
    return (
        <table>
            <tr>
                <th>invite_id</th>
                <th>sender</th>
                <th>sig_id</th>
                <th>invite</th>
                <th>vector</th>
                <th>invite_time</th>
                <th>user_id</th>
                <th>status</th>
            </tr>
            {props.invites.map((invite) => {
                return (
                    <tr className={invite.status == 'read' ? 'read' : 'unread'}>
                        <td>{invite.invite_id}</td>
                        <td>{invite.sender_id}</td>
                        <td>{invite.sig_id}</td>
                        <td>{invite.invite}</td>
                        <td>{invite.vector}</td>
                        <td>{invite.invite_time}</td>
                        <td>{invite.user_id}</td>
                        <td>{invite.status}</td>
                    </tr>
                )
            })}
        </table>
    )
}
